package com.ruoyi.common.core.web.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ruoyi.common.core.api.RemoteTranNewService;
import com.ruoyi.common.core.constant.HttpStatus;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.domain.SysDictDataVo;
import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.utils.sql.SqlUtil;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.PageDomain;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.core.web.page.TableSupport;
import com.ruoyi.common.translate.annotation.TranslateAnnotation;
import com.ruoyi.common.translate.enums.TranseTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * web层通用数据处理
 * 
 * @author ruoyi
 */
public class BaseController {
    protected final Logger logger = LoggerFactory.getLogger(BaseController.class);
    @Autowired
    private RemoteTranNewService remoteTranNewService;

    /**
     * 描述：将翻译后的值 赋给对应字段
     * 备注：
     * 日期： 10:07 2019/12/5
     * 作者： zrd
     *
     * @param obj   原始实体
     * @param filed 待翻译字段
     * @param value 翻译后的值
     * @return java.lang.Object
     **/
    private static Object taransFilds(Object obj, String filed, String value) {
        try {
            Class<?> c = obj.getClass();
            Field field = c.getDeclaredField(filed);
            //获取字段
            field.setAccessible(true);
            field.set(obj, value);
            //为字段赋值
        } catch (Exception e) {
//            e.printStackTrace();

        }
        return obj;
    }

    /**
     * 响应请求分页数据
     */
    protected TableDataInfo getDataTable(List<?> list, Class tClass) {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setTotal(new PageInfo(list).getTotal());
        list = translateBeanList(list, tClass);
        rspData.setRows(list);
        return rspData;
    }
    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder)
    {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport()
        {
            @Override
            public void setAsText(String text)
            {
                setValue(DateUtils.parseDate(text));
            }
        });
    }

    /**
     * 设置请求分页数据
     */
    protected void startPage()
    {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize))
        {
            String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
            Boolean reasonable = pageDomain.getReasonable();
            PageHelper.startPage(pageNum, pageSize, orderBy).setReasonable(reasonable);
        }
    }

    /**
     * 响应请求分页数据
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected TableDataInfo getDataTable(List<?> list)
    {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(HttpStatus.SUCCESS);
        rspData.setRows(list);
        rspData.setMsg("查询成功");
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

    /**
     * 响应返回结果
     * 
     * @param rows 影响行数
     * @return 操作结果
     */
    protected AjaxResult toAjax(int rows)
    {
        return rows > 0 ? AjaxResult.success() : AjaxResult.error();
    }

    /**
     * 响应返回结果
     *
     * @param result 结果
     * @return 操作结果
     */
    protected AjaxResult toAjax(boolean result)
    {
        return result ? success() : error();
    }

    /**
     * 返回成功
     */
    public AjaxResult success()
    {
        return AjaxResult.success();
    }

    /**
     * 返回失败消息
     */
    public AjaxResult error()
    {
        return AjaxResult.error();
    }

    /**
     * 返回成功消息
     */
    public AjaxResult success(String message)
    {
        return AjaxResult.success(message);
    }

    /**
     * 返回失败消息
     */
    public AjaxResult error(String message)
    {
        return AjaxResult.error(message);
    }

    /**
     * 描述：  单实体翻译工具类
     * 备注：
     * 日期： 10:08 2019/12/5
     * 作者： zrd
     *
     * @param obj
     * @return java.lang.Object
     **/
    private Object valid(Object obj, Class<?> clazz) {
        Field[] fields = ReflectUtil.getFields(clazz);
        for (Field field : fields) {
            TranslateAnnotation translateAnnotation = field.getAnnotation(TranslateAnnotation.class);//获取属性上的@Test注解
            if (translateAnnotation != null) {
                field.setAccessible(true);//设置属性可访问
                String reslut = "";
                //原始值
                try {
                    String objStr = field.get(obj).toString();
                    TranseTypeEnum transeTypeEnum = translateAnnotation.distType();
                    switch (transeTypeEnum) {
                        case DIST:
                            reslut = getDistMapByType(translateAnnotation.distCode(), objStr);
                            break;
                        case MULTIPLE_DIST:
                            reslut = getMutDistVale(translateAnnotation.distCode(), objStr);
                            break;


                        case DATE:

                            reslut = DateUtils.parseDateToStr(translateAnnotation.distCode(), (Date) field.get(obj));

                            break;
                        default:
                            reslut = "";
                            break;
                    }
                } catch (Exception e) {
                    reslut = "";
                }
                obj = taransFilds(obj, translateAnnotation.filed(), reslut);

            }
        }
        return obj;
    }


    /**
     * 描述：获取 数据字典
     * 备注：
     * 日期： 11:43 2020/1/8
     * 作者： zrd
     *
     * @param type
     * @return java.util.Map<java.lang.String, java.lang.String>
     **/
    public String getDistMapByType(String type, String keyValue) {
        String result = keyValue;
        R<List<SysDictDataVo>> rs = remoteTranNewService.getAllDictData(type);

        List<SysDictDataVo> sysDictDataList = rs.getData();

        if (sysDictDataList != null && sysDictDataList.size() > 0) {
            for (SysDictDataVo sysDictData : sysDictDataList) {
                if (sysDictData.getDictValue().equals(keyValue)) {
                    result = sysDictData.getDictLabel();
                    break;
                }
            }

        }


        return result;
    }

    /**
     * 描述：  获取多选下拉框的翻译
     * 备注：
     * 日期： 11:08 2020/6/3
     * 作者： zrd
     *
     * @param type
     * @param keyValue
     * @return java.lang.String
     **/
    public String getMutDistVale(String type, String keyValue) {
        String result = "";
        R<List<SysDictDataVo>> rs = remoteTranNewService.getAllDictData(type);

        List<SysDictDataVo> sysDictDataList = rs.getData();
        if (StrUtil.isNotBlank(keyValue)) {
            String[] strArr = keyValue.split(",");
            for (String str : strArr) {
                if (sysDictDataList != null && sysDictDataList.size() > 0) {
                    for (SysDictDataVo sysDictData : sysDictDataList) {
                        if (sysDictData.getDictValue().equals(str)) {
                            result += sysDictData.getDictLabel() + ",";
                            break;
                        }
                    }

                }
            }
        }
//如果结果为空 则不翻译
        if (StrUtil.isBlank(result)) {
            result = keyValue;
        }


        return result;
    }


    /**
     * 描述：  翻译list
     * 备注：
     * 日期： 15:51 2020/7/29
     * 作者： zrd
     *
     * @param list
     * @param tClass
     * @return java.util.List
     **/
    public <T> List translateBeanList(List<T> list, Class tClass) {
        if (CollUtil.isEmpty(list)) {
            return new ArrayList();
        }
        List<T> rsList = new ArrayList();
        for (T t : list
        ) {
            rsList.add((T) valid(t, tClass));
        }
        return rsList;
    }

    /**
     * 描述：  翻译实体
     * 备注：
     * 日期： 15:51 2020/7/29
     * 作者： zrd
     *
     * @param t
     * @param tClass
     * @return T
     **/
    public <T> T translateBean(T t, Class tClass) {
        return (T) valid(t, tClass);
    }

}
