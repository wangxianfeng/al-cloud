package com.ruoyi.gen.util;

/**
 * @author: zdc
 * @Date: 2020/5/25 16:14
 * @Description:
 */
public enum TemplateTypeEnum {

    //技术合同审批
    JAVA("1", "JAVA文件"),
    VUE("3", "VUE文件"),
    JS("4", "JS文件"),
    SQL("5", "SQL文件"),
    //生产项目立项申请单
    XML("2", "XML文件");


    private String key;

    private String value;

    TemplateTypeEnum(String i, String value) {
        this.key = i;
        this.value = value;
    }

    /**
     * 描述：   通过枚举code 获取枚举值
     * 备注：
     * 日期： 15:57 2019/11/20
     * 作者： zrd
     *
     * @param i
     * @return java.lang.String
     **/
    public static String getValue(String i) {
        for (TemplateTypeEnum ele : values()) {
            if (i.equals(ele.getKey())) {
                return ele.getValue();
            }
        }
        return null;
    }

    public static TemplateTypeEnum getEnumByStep(String i) {
        for (TemplateTypeEnum ele : values()) {
            if (i.equals(ele.getKey())) {
                return ele;
            }
        }
        return null;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
